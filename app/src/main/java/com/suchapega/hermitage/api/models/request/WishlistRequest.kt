package com.suchapega.hermitage.api.models.request

data class WishlistRequest(val user_token: String, val wishlist_id: Int)