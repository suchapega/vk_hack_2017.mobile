package com.suchapega.hermitage.api.models.request

import com.suchapega.hermitage.api.models.response.Position

data class PositionRequest(val user_token: String?, val position: Position)