package com.suchapega.hermitage.api.enums

enum class AvailabilityEnum(val value: Int) {
    Unknown(0), Low(1), Medium(2), Hight(3)
}