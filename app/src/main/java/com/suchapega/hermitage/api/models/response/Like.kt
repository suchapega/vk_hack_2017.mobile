package com.suchapega.hermitage.api.models.response

data class Like(val user_token: String, val exhibit_id: Int, val like: Boolean)