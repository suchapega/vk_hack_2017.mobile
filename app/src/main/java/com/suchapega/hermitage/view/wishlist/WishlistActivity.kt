package com.suchapega.hermitage.view.wishlist

import android.support.v7.app.AppCompatActivity
import android.os.Bundle

import com.suchapega.hermitage.R

class WishlistActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_wishlist)
    }
}
