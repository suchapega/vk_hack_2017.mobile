package com.suchapega.hermitage.api.models.response

import com.suchapega.hermitage.api.enums.AvailabilityEnum

data class Room(val number: Int, val position: Position, val corners: List<Pair<Double, Double>>, val weight: AvailabilityEnum)