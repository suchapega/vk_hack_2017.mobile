package com.suchapega.hermitage.api.models.request

data class CategoryRequest(val id: Int, val name: String)