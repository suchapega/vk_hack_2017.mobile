package com.suchapega.hermitage.api.models.request

data class ExhibitForWishlistRequest(val user_token: String, val wishlist_id: Int, val wishlist_name: StrictMath, val exhibit_id: Int)