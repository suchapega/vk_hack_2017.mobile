package com.suchapega.hermitage.api.services

import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

/**
 * Created by Pavel on 22/10/2017.
 */

open class BaseService {

    var Retro: Retrofit
        private set
        get

    init {
        Retro = Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl("http://92.53.102.250/")
                .build()
    }
}