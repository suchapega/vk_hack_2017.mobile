package com.suchapega.hermitage.api.services

import com.suchapega.hermitage.api.models.response.Availability
import com.suchapega.hermitage.api.models.response.Room
import retrofit2.Call
import retrofit2.http.GET

/**
 * Created by Pavel on 22/10/2017.
 */

interface IAvailabilityService {

    @GET("weight")
    fun GetTotalAvailability(): Call<Availability>

    @GET("weight/rooms")
    fun GetRoomsAvailability(): Call<List<Room>>
}

class AvailabilityService: BaseService() {

    fun GetTotalAvailability(): Call<Availability> =
            Retro.create(IAvailabilityService::class.java).GetTotalAvailability()

    fun GetRoomsAvailability(): Call<List<Room>> =
            Retro.create(IAvailabilityService::class.java).GetRoomsAvailability()
}