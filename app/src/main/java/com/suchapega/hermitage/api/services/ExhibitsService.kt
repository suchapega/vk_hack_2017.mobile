package com.suchapega.hermitage.api.services

import com.suchapega.hermitage.api.models.request.ExhibitRequest
import com.suchapega.hermitage.api.models.response.Exhibit
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Path

/**
 * Created by Pavel on 22/10/2017.
 */

interface IExhibitsService {

    @GET("/exhibits/{id}")
    fun GetExhibit(@Path("id") id: Int): Call<Exhibit>

    @POST("/exhibits/}")
    fun GetExhibits(): Call<List<Exhibit>>

    @POST("/like}")
    fun SetLike(exhibit: ExhibitRequest): Call<Void>
}

class ExhibitService: BaseService() {

    fun GetExhibit(id: Int): Call<Exhibit> =
            Retro.create(IExhibitsService::class.java).GetExhibit(id)

    fun GetExhibits(): Call<List<Exhibit>> =
            Retro.create(IExhibitsService::class.java).GetExhibits()

    fun SetLike(exhibit: ExhibitRequest): Call<Void> =
            Retro.create(IExhibitsService::class.java).SetLike(exhibit)
}