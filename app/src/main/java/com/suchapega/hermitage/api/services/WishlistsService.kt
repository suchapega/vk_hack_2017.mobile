package com.suchapega.hermitage.api.services

import com.suchapega.hermitage.api.models.request.ExhibitForWishlistRequest
import com.suchapega.hermitage.api.models.request.WishlistRequest
import com.suchapega.hermitage.api.models.response.Exhibit
import com.suchapega.hermitage.api.models.response.Wishlist
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.POST

/**
 * Created by Pavel on 22/10/2017.
 */

interface IWishlistService {

    @POST("/wishlistExhibit/add}")
    fun AddExhibitToWishlist(exhibit: ExhibitForWishlistRequest): Call<Void>

    @GET("/wishlists")
    fun GetWishlists(userToken: String): Call<List<Wishlist>>

    @POST("/wishlistExhibits}")
    fun GetWishlistExhibits(wishlist: WishlistRequest): Call<List<Exhibit>>
}

class WishlistService: BaseService() {

    fun AddExhibitToWishlist(exhibit: ExhibitForWishlistRequest): Call<Void> =
            Retro.create(IWishlistService::class.java).AddExhibitToWishlist(exhibit)

    fun GetWishlists(userToken: String): Call<List<Wishlist>> =
            Retro.create(IWishlistService::class.java).GetWishlists(userToken)

    fun GetWishlistExhibits(wishlist: WishlistRequest): Call<List<Exhibit>> =
            Retro.create(IWishlistService::class.java).GetWishlistExhibits(wishlist)
}