package com.suchapega.hermitage.api.services

import com.suchapega.hermitage.api.models.request.AuthRequest
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.POST

/**
 * Created by Pavel on 21/10/2017.
 */

interface IAuthService {

    @POST("auth")
    fun InitAuth(@Body auth: AuthRequest): Call<Void>
}

class AuthService: BaseService() {

    fun InitAuth(userToken: String): Call<Void> =
            Retro.create(IAuthService::class.java).InitAuth(AuthRequest(userToken))
}