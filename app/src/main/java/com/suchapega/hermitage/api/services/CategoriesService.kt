package com.suchapega.hermitage.api.services

import com.suchapega.hermitage.api.models.request.CategoryRequest
import retrofit2.Call
import retrofit2.http.GET

/**
 * Created by Ars on 22.10.2017.
 */

interface ICategoriesService {

    @GET("/categories")
    fun GetCategories(): Call<List<CategoryRequest>>
}

class CategoriesService: BaseService() {

    fun GetCategories(): Call<List<CategoryRequest>> =
            Retro.create(ICategoriesService::class.java).GetCategories()
}