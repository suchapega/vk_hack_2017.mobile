package com.suchapega.hermitage.api.services

import com.suchapega.hermitage.api.models.request.PositionRequest
import com.suchapega.hermitage.api.models.response.Exhibit
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.POST

/**
 * Created by Pavel on 22/10/2017.
 */

interface IRecommendationsService {

    @POST("recommendation")
    fun GetRecommendations(@Body position: PositionRequest): Call<List<Exhibit>>
}

class RecommendationsService: BaseService() {

    fun GetRecommendations(position: PositionRequest): Call<List<Exhibit>> =
            Retro.create(IRecommendationsService::class.java).GetRecommendations(position)
}