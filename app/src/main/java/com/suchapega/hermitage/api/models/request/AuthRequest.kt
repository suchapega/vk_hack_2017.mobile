package com.suchapega.hermitage.api.models.request

data class AuthRequest(val user_token: String)