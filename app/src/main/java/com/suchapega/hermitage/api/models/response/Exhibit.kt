package com.suchapega.hermitage.api.models.response

data class Exhibit(val id: Int, val name: String, val author: String, val image_url: String, val collection: String, val place: String, val year: String, val material: String, val technique: String, val description: String, val room: Room, val like: Boolean)