package com.suchapega.hermitage.view

/**
 * Created by Pavel on 21/10/2017.
 */

import android.Manifest
import android.animation.ValueAnimator
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.os.Looper
import android.support.v4.app.ActivityCompat
import android.support.v4.app.DialogFragment
import android.support.v4.app.FragmentActivity
import android.support.v7.app.AlertDialog
import android.util.Log
import android.view.View
import android.widget.*
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.*
import com.indooratlas.android.sdk.IALocation
import com.indooratlas.android.sdk.IALocationManager
import com.indooratlas.android.sdk.IALocationRequest
import com.indooratlas.android.sdk.IARegion
import com.indooratlas.android.sdk.resources.IAFloorPlan
import com.indooratlas.android.sdk.resources.IALocationListenerSupport
import com.indooratlas.android.sdk.resources.IAResourceManager
import com.indooratlas.android.sdk.resources.IATask
import com.squareup.picasso.Picasso
import com.squareup.picasso.Target
import com.suchapega.hermitage.R
import com.suchapega.hermitage.api.enums.AvailabilityEnum.*
import com.suchapega.hermitage.api.models.request.PositionRequest
import com.suchapega.hermitage.api.models.response.Availability
import com.suchapega.hermitage.api.models.response.Exhibit
import com.suchapega.hermitage.api.models.response.Position
import com.suchapega.hermitage.api.models.response.Room
import com.suchapega.hermitage.api.services.AuthService
import com.suchapega.hermitage.api.services.AvailabilityService
import com.suchapega.hermitage.api.services.GeolocationService
import com.suchapega.hermitage.api.services.RecommendationsService
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*


class MainActivity : FragmentActivity(), OnMapReadyCallback {

    private val MAP_PERMISSIONS = 142
    private val INDOORATLAS_PERMISSIONS = 42

    private val HUE_IABLUE = 200.0f

    /* used to decide when bitmap should be downscaled */
    private val MAX_DIMENSION = 20482

    private var mMap: GoogleMap? = null

    private var mMarker: Marker? = null
    private var mOverlayFloorPlan: IARegion? = null
    private var mGroundOverlay: GroundOverlay? = null
    private var mIALocationManager: IALocationManager? = null
    private var mResourceManager: IAResourceManager? = null
    private var mFetchFloorPlanTask: IATask<IAFloorPlan>? = null
    private var mLoadTarget: Target? = null
    private var mCameraPositionNeedsUpdating = true // update on first location


    private var mUserToken: String? = null
    private var mLastPosition: Position? = null

    private var mProgressBar: ProgressBar? = null

    private var mDrawAvailabilityLayer = false
    private var mAvailabilityRooms: List<Room>? = listOf()
    private var mAvailabilityPolygons: MutableList<Polygon> = mutableListOf()

    private var mSelectedFloor = 1
    var SelectedFloor
        get() = mSelectedFloor
        set(value) {
            if (value < 1 || value > 3)
                return

            mSelectedFloor = value

            val selectedFloorId = resources.getIdentifier("indooratlas_floor_plan_id_${value}", "string", this.packageName)
            fetchFloorPlan(getString(selectedFloorId))
        }

    //<editor-fold desk="IndoorAtlas overlay">

    private val MAX_LOCATION_UPDATE_COUNT = 50
    private var mLocationUpdateCount = 0
    /**
     * Listener that handles location change events.
     */
    private val mListener = object : IALocationListenerSupport() {

        /**
         * Location changed, move marker and camera position.
         */
        override fun onLocationChanged(location: IALocation?) {

            if (mMap == null || location == null) {
                // location received before map is initialized, ignoring update here
                return
            }

            Log.d("IndoorAtlas", "new location received with coordinates: ${location.latitude}, ${location.longitude}. Floor: ${location.floorLevel}")


            val latLng = LatLng(location.latitude, location.longitude)
            if (mMarker == null) {
                // first location, add marker
                mMarker = mMap?.addMarker(MarkerOptions().position(latLng)
                        .icon(BitmapDescriptorFactory.defaultMarker(HUE_IABLUE)))
            } else {
                // move existing markers position to received location
                mMarker?.setPosition(latLng)
            }

            // our camera position needs updating if location has significantly changed
            if (mCameraPositionNeedsUpdating) {
                mMap?.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 17.5f))
                mCameraPositionNeedsUpdating = false
            }

            var floor = location.floorLevel
            if (floor < 1 || floor > 3)
                floor = 1

            mLastPosition = Position(floor, location.latitude, location.longitude)

            mLocationUpdateCount++
            if (mLocationUpdateCount < MAX_LOCATION_UPDATE_COUNT)
                return

            mLocationUpdateCount = 0

            GeolocationService().UpdateGeolocation(PositionRequest(mUserToken, Position(location.floorLevel, location.latitude, location.longitude))).enqueue(object : Callback<Void> {
                override fun onResponse(call: Call<Void>, response: Response<Void>) {
                    //do nothing
                }

                override fun onFailure(call: Call<Void>, t: Throwable) {
                    Log.d("API", "Failure: ${t}")
                }
            })
        }
    }

    /**
     * Listener that changes overlay if needed
     */
    private val mRegionListener = object : IARegion.Listener {

        override fun onEnterRegion(region: IARegion) {
            if (region.type == IARegion.TYPE_FLOOR_PLAN) {
                val newId = region.id
                // Are we entering a new floor plan or coming back the floor plan we just left?
                if (mGroundOverlay == null || region != mOverlayFloorPlan) {
                    mCameraPositionNeedsUpdating = true // entering new fp, need to move camera
                    if (mGroundOverlay != null) {
                        mGroundOverlay!!.remove()
                        mGroundOverlay = null
                    }
                    mOverlayFloorPlan = region // overlay will be this (unless error in loading)
                    fetchFloorPlan(newId)
                } else {
                    mGroundOverlay?.transparency = 0.0f
                }
            }
        }

        override fun onExitRegion(region: IARegion) {
            if (mGroundOverlay != null) {
                // Indicate we left this floor plan but leave it there for reference
                // If we enter another floor plan, this one will be removed and another one loaded
                mGroundOverlay!!.transparency = 0.5f
            }
        }

    }

    override fun onDestroy() {
        super.onDestroy()

        // remember to clean up after ourselves
        mIALocationManager?.destroy()
    }

    override fun onResume() {
        super.onResume()

        // start receiving location updates & monitor region changes
        mIALocationManager?.requestLocationUpdates(IALocationRequest.create(), mListener)
        mIALocationManager?.registerRegionListener(mRegionListener)
    }

    override fun onPause() {
        super.onPause()

        // unregister location & region changes
        mIALocationManager?.removeLocationUpdates(mListener)
        mIALocationManager?.registerRegionListener(mRegionListener)
    }


    /**
     * Sets bitmap of floor plan as ground overlay on Google Maps
     */
    private fun setupGroundOverlay(floorPlan: IAFloorPlan, bitmap: Bitmap?) {

        if (mGroundOverlay != null) {
            mGroundOverlay!!.remove()
        }

        if (mMap != null) {
            val bitmapDescriptor = BitmapDescriptorFactory.fromBitmap(bitmap)
            val iaLatLng = floorPlan.center
            val center = LatLng(iaLatLng.latitude, iaLatLng.longitude)
            val fpOverlay = GroundOverlayOptions()
                    .image(bitmapDescriptor)
                    .position(center, floorPlan.widthMeters, floorPlan.heightMeters)
                    .bearing(floorPlan.bearing)

            mGroundOverlay = mMap!!.addGroundOverlay(fpOverlay)
        }
    }

    /**
     * Download floor plan using Picasso library.
     */
    private fun fetchFloorPlanBitmap(floorPlan: IAFloorPlan) {

        val url = floorPlan.url

        if (mLoadTarget == null) {
            mLoadTarget = object : Target {

                override fun onBitmapLoaded(bitmap: Bitmap?, from: Picasso.LoadedFrom) {
                    Log.d("IndoorAtlas", "onBitmap loaded with dimensions: ${bitmap?.width} × ${bitmap?.height}")
                    setupGroundOverlay(floorPlan, bitmap)
                }

                override fun onPrepareLoad(placeHolderDrawable: Drawable?) {
                    // N/A
                }

                override fun onBitmapFailed(placeHolderDraweble: Drawable?) {
//                    SelectedFloor = 1
                    mOverlayFloorPlan = null
                }
            }
        }

        val request = Picasso.with(this).load(url)

        val bitmapWidth = floorPlan.bitmapWidth
        val bitmapHeight = floorPlan.bitmapHeight

        if (bitmapHeight > MAX_DIMENSION) {
            request.resize(0, MAX_DIMENSION)
        } else if (bitmapWidth > MAX_DIMENSION) {
            request.resize(MAX_DIMENSION, 0)
        }

        request.into(mLoadTarget)

        DrawFloorAvailabilityLayer(SelectedFloor)
    }


    /**
     * Fetches floor plan data from IndoorAtlas server.
     */
    private fun fetchFloorPlan(id: String) {

        // if there is already running task, cancel it
        cancelPendingNetworkCalls()

        val task = mResourceManager?.fetchFloorPlanWithId(id)

        task?.setCallback({ result ->
            if (result.isSuccess && result.result != null) {
                // retrieve bitmap for this floor plan metadata
                fetchFloorPlanBitmap(result.result)
            } else {
                // ignore errors if this task was already canceled
                if (!task.isCancelled) {
//                    SelectedFloor = 1
                    mOverlayFloorPlan = null
                }
            }
        }, Looper.getMainLooper()) // deliver callbacks using main looper

        // keep reference to task so that it can be canceled if needed
        mFetchFloorPlanTask = task
    }

    /**
     * Helper method to cancel current task if any.
     */
    private fun cancelPendingNetworkCalls() {
        if (mFetchFloorPlanTask != null && !mFetchFloorPlanTask!!.isCancelled) {
            mFetchFloorPlanTask!!.cancel()
        }
    }

    //</editor-fold>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

//        mProgressBar = findViewById(R.id.spin_kit) as ProgressBar?
        mProgressBar?.visibility = View.INVISIBLE

        auth()
        initMap()
        initFloorButtons()
        initAvailability()
        initLayersButton()
        initBottomButtons()

        val neededPermissions = arrayOf(
                Manifest.permission.CHANGE_WIFI_STATE,
                Manifest.permission.ACCESS_WIFI_STATE,
                Manifest.permission.ACCESS_COARSE_LOCATION)
        ActivityCompat.requestPermissions(this, neededPermissions, INDOORATLAS_PERMISSIONS)
    }

    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap

        if (mMap == null)
            return

        mMap!!.uiSettings.isCompassEnabled = false
        mMap!!.uiSettings.isMyLocationButtonEnabled = false
        mMap!!.isMyLocationEnabled = true
    }

    private fun auth() {
        val sharedPref = getSharedPreferences(getString(R.string.preference_file_key), Context.MODE_PRIVATE)
        mUserToken = sharedPref.getString(getString(R.string.preference_user_token), null)

        if (mUserToken == null) {
            mUserToken = UUID.randomUUID().toString()

            val editor = sharedPref.edit()
            editor.putString(getString(R.string.preference_user_token), mUserToken)
            editor.commit()
        }

        AuthService().InitAuth(mUserToken!!).enqueue(object : Callback<Void> {
            override fun onResponse(call: Call<Void>, response: Response<Void>) {
                //do nothing
            }

            override fun onFailure(call: Call<Void>, t: Throwable) {
                Log.d("API", "Failure: ${t}")
            }
        })
    }

    private fun initMap() {
        val mapFragment = supportFragmentManager.findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)

        // instantiate IALocationManager and IAResourceManager
        mIALocationManager = IALocationManager.create(this)
        mResourceManager = IAResourceManager.create(this)
    }

    private fun initFloorButtons() {
        val selectedFloorLayer = findViewById(R.id.floor_selection) as RelativeLayout
        val floorButtonsLayer = findViewById(R.id.floor_buttons) as LinearLayout
        for (floor in 1..floorButtonsLayer.childCount) {
            val floorBtn = floorButtonsLayer.getChildAt(floor - 1)
            floorBtn.setOnClickListener {

                val selectionHeight = resources.getDimensionPixelSize(R.dimen.white_button_height)
                val changeSelectionAnimation = ValueAnimator.ofInt(
                        (SelectedFloor - 1) * selectionHeight,
                        (floor - 1) * selectionHeight)

                changeSelectionAnimation.addUpdateListener {
                    val lp = selectedFloorLayer.getLayoutParams() as RelativeLayout.LayoutParams
                    lp.setMargins(0, it.animatedValue as Int, 0, 0)
                    selectedFloorLayer.layoutParams = lp
                }
                changeSelectionAnimation.duration = 200
                changeSelectionAnimation.start()

                SelectedFloor = floor
            }
        }
    }

    private fun initAvailability() {
        mProgressBar?.visibility = View.VISIBLE
        val availabilityIndicator = findViewById(R.id.availability_indicator) as FrameLayout
        AvailabilityService().GetTotalAvailability().enqueue(object : Callback<Availability> {
            override fun onResponse(call: Call<Availability>, response: Response<Availability>) {
                if (response.isSuccessful) {

                    val availabilityLevel = response.body()!!.weigh
                    var availabilityDrawableId = 0
                    when (availabilityLevel) {
                        Low -> availabilityDrawableId = R.drawable.availability_indicator_low
                        Medium -> availabilityDrawableId = R.drawable.availability_indicator_medium
                        Hight -> availabilityDrawableId = R.drawable.availability_indicator_hight
                    }

                    availabilityIndicator.setBackgroundResource(availabilityDrawableId)
                }
                mProgressBar?.visibility = View.INVISIBLE
            }

            override fun onFailure(call: Call<Availability>, t: Throwable) {
                Log.d("API", "Failure: ${t}")
            }
        })

        val availabilityButton = findViewById(R.id.availability_button) as Button
        availabilityButton.setOnClickListener { ShowHideAvailabilityLayer() }
    }

    private fun ShowHideAvailabilityLayer() {
        mDrawAvailabilityLayer = !mDrawAvailabilityLayer
        if (mDrawAvailabilityLayer) {
            mProgressBar?.visibility = View.VISIBLE
            AvailabilityService().GetRoomsAvailability().enqueue(object : Callback<List<Room>> {
                override fun onResponse(call: Call<List<Room>>, response: Response<List<Room>>) {
                    if (response.isSuccessful && mMap != null) {
                        mAvailabilityRooms = response.body()
                        DrawFloorAvailabilityLayer(SelectedFloor)
                    }
                    mProgressBar?.visibility = View.INVISIBLE
                }

                override fun onFailure(call: Call<List<Room>>, t: Throwable) {
                    Log.d("API", "Failure: ${t}")
                }
            })
        }
        else {
            ClearAvailabilityLayer()
        }
    }

    private fun DrawFloorAvailabilityLayer(floor: Int) {
        ClearAvailabilityLayer()
        val rooms = mAvailabilityRooms?.filter { it.position.floor == SelectedFloor }
        if (rooms == null || rooms.isEmpty())
            return

        for (room in rooms) {
            val roomOpt = PolygonOptions()

            var availabilityColorId = 0
            when (room.weight) {
                Low -> availabilityColorId = R.color.availability_room_low
                Medium -> availabilityColorId = R.color.availability_room_medium
                Hight -> availabilityColorId = R.color.availability_room_hight
            }
            roomOpt.fillColor(resources.getColor(availabilityColorId))
            roomOpt.strokeColor(resources.getColor(availabilityColorId))

            for (roomCorner in room.corners) {
                roomOpt.add(LatLng(roomCorner.first, roomCorner.second))
            }

            mAvailabilityPolygons.add(mMap!!.addPolygon(roomOpt))
        }
    }

    private fun CheckRecomendations() {
        if (mLastPosition == null)
            return

        mProgressBar?.visibility = View.VISIBLE
        RecommendationsService().GetRecommendations(PositionRequest(mUserToken, mLastPosition!!)).enqueue(object : Callback<List<Exhibit>> {
            override fun onResponse(call: Call<List<Exhibit>>, response: Response<List<Exhibit>>) {
                if (response.isSuccessful) {

                    val exhibits = response.body()
                    if (exhibits != null && exhibits.isNotEmpty()) {
                        val index = Random().nextInt(exhibits.count())
                        val exhibit = exhibits[index]
                        ShowAlert("'${exhibit.name}'\nАвтор: ${exhibit.author}", "Рекомендуем вам посмотреть")
                    }
                }
                mProgressBar?.visibility = View.INVISIBLE
            }

            override fun onFailure(call: Call<List<Exhibit>>, t: Throwable) {
                Log.d("API", "Failure: ${t}")
            }
        })
    }

    private fun ClearAvailabilityLayer() {
        if (mAvailabilityPolygons.isEmpty())
            return

        for (polygon in mAvailabilityPolygons) {
            polygon.remove()
        }
        mAvailabilityPolygons.clear()
    }

    private fun initLayersButton() {
        val layersButton = findViewById(R.id.layers_button) as ImageButton
    }

    private fun initBottomButtons() {
        val infoButton = findViewById(R.id.info_button) as ImageButton
        infoButton.setOnClickListener {
//            val intent = Intent(this, InfoActivity::class.java)
//            startActivity(intent)

            CheckRecomendations()
        }

//        val wishlistButton = findViewById(R.id.wishlist_button) as ImageButton
//        wishlistButton.setOnClickListener {
//            val intent = Intent(this, WishlistsActivity::class.java)
//            startActivity(intent)
//        }

//        val searchButton = findViewById(R.id.search_button) as ImageButton
//        searchButton.setOnClickListener {
//            val intent = Intent(this, SearchActivity::class.java)
//            startActivity(intent)
//        }

//        val catalogButton = findViewById(R.id.catalog_button) as ImageButton
//        catalogButton.setOnClickListener {
//            val intent = Intent(this, CategoriesActivity::class.java)
//            startActivity(intent)
//        }
    }

    private fun ShowAlert(text: String, title: String = "") {
        runOnUiThread {
            val builder = AlertDialog.Builder(this)
            builder.setMessage(text)
            builder.setTitle(title)
            builder.setNegativeButton(R.string.ok_button_title, DialogInterface.OnClickListener { dialog, which -> {} })
            builder.create().show()
        }
    }
}