package com.suchapega.hermitage.api.models.response

data class Position (val floor: Int, val latitude: Double, val longitude: Double)

