package com.suchapega.hermitage.api.services


import com.suchapega.hermitage.api.models.request.PositionRequest
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.POST

/**
 * Created by Pavel on 22/10/2017.
 */

interface IGeolocationService {

    @POST("/update_position")
    fun UpdateGeolocation(@Body position: PositionRequest): Call<Void>
}

class GeolocationService: BaseService() {

    fun UpdateGeolocation(position: PositionRequest): Call<Void> =
            Retro.create(IGeolocationService::class.java).UpdateGeolocation(position)
}