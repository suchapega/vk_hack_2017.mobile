# Suchapega Hermitage

#### Android app for The State Hermitage Museum indoor navigation
It was developed during "VK Hackathon"

##### Technology stack

* Indoor Atlas SDK for quality indoor navigation without any additional hardware
* MySQL Jaws DB for storage rooms coordinates and routes between them
* Ktor framework for REST API

|Map on the main screen as a main app purpose|Collection of favorite art with function to get a route|
|-|-|
|![Enter report by voice](Images/1.png)|![Enter report by text](Images/2.png)|

|Indoor Atlas SDK give access to navigation coverage|... in different diapasons|
|-|-|
|![Enter report by voice](Images/3.png)|![Enter report by text](Images/4.png)|
