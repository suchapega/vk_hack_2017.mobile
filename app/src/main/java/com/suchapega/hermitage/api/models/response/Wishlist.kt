package com.suchapega.hermitage.api.models.response

data class Wishlist(val user_token: String, val wishlist_id : Int, val wishlist_name: String , val exhibit_id : Int)


