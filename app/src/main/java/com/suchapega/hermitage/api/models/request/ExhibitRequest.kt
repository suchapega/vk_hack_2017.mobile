package com.suchapega.hermitage.api.models.request

data class ExhibitRequest(val user_token: String, val exhibit_id : Int)