package com.suchapega.hermitage.api.models.response

data class ExhibitShort (val id: Int, val name: String, val author: String, val image_url : String)