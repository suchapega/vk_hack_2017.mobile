package com.suchapega.hermitage.api.models.response

data class WishlistShort (val id: Int, val Name: String)