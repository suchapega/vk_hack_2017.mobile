package com.suchapega.hermitage.api.models.response

import com.suchapega.hermitage.api.enums.AvailabilityEnum

data class Availability(val weigh: AvailabilityEnum)